#include "shape.h"
#include <cmath>
//
//NAMED
//

std::map<std::string, int> Named::names;

Named::Named(){
}

Named::Named(std::string name){
	auto curr = names.insert(std::make_pair(name, 0));
	curr.first->second++;
	it = curr.first;
	number = it->second;
}

std::string Named::getName()const{
	return it->first + "." + std::to_string(number);
}

//
// COLORED 
//

Colored::Colored(){
	red = green = blue = 0;
}

Colored::Colored(int r, int g, int b){
	red = r;
	green = g;
	blue = b;
}

Colored::Colored(std::string strColor){
	red = 0;
	green = 0;
	blue = 0;
	//TODO
}

int Colored::Red()const{
	return red;
}

int Colored::Green()const{
	return green;
}

int Colored::Blue()const{
	return blue;
}

//
//SEGMENT
//

Segment::Segment(int xs, int ys, int xe, int ye){
	x1 = xs;
	y1 = ys;
	x2 = xe;
	y2 = ye;
}
 
//
//SHAPE
//

//
//RECTANGLE
//


std::vector<Segment> Rectangle::getSegments()const{
	std::vector<Segment> sgm;
	sgm.push_back(Segment(x1, y1, x1, y2));
	sgm.push_back(Segment(x1, y2, x2, y2));
	sgm.push_back(Segment(x2, y2, x2, y1));
	sgm.push_back(Segment(x2, y1, x1, y1));
	return sgm;
}

double Rectangle::square() const{
	return (x2 - x1) * (y2 - y1);
}

//
//CIRCLE
//


std::vector<Segment> Circle::getSegments()const{
	double arg = 0.01;
	int x0, y0, x1, y1;
	std::vector<Segment> sgm;
	x0 = x + r;
	y0 = y;
	while(arg <= 6.3){
		x1 = (x + ((double)r)*cos(arg));
		y1 = (y + ((double)r)*sin(arg));
		sgm.push_back(Segment(x0, y0, x1, y1));
		x0 = x1;
		y0 = y1;
		arg += 0.01;
	}
	return sgm;
}

double Circle::square()const{
	return r * r * 3.14;
}

//
//TRIANGLE
//


std::vector<Segment> Triangle::getSegments()const{
	std::vector<Segment> sgm;
	sgm.push_back(Segment(x1, y1, x2, y2));
	sgm.push_back(Segment(x2, y2, x3, y3));
	sgm.push_back(Segment(x3, y3, x1, y1));
	return sgm;
}

double Triangle::square() const{
	return ((x1 - x3)*(y2 - y3) - (y1- y3)*(x2 - x3))/2;
}

//
//ETC
//

void draw(image_drawer& drawer,const Shape& shp){
	std::vector<Segment> sgm;
	sgm = shp.getSegments();
	drawer.pen_color(shp.Red(), shp.Green(), shp.Blue());

	for (auto& sm : sgm){
		drawer.line_segment(sm.x1, sm.y1, sm.x2, sm.y2);
	}
}

