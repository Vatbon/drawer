#ifndef SHAPE_H
#define SHAPE_H

#include <map>
#include <vector>
#include <string>
#include "bitmap/bitmap_image.hpp"
class Named{
	private:
		static std::map<std::string, int> names;
		std::map<std::string, int>::const_iterator it;
	protected:
		int number;
	public:
		Named();
		Named(std::string);
		std::string getName()const;
};

class Colored{
	protected:
		int red, green, blue;
	public:
		Colored();
		Colored(int, int, int);
		Colored(std::string);
		int Red()const;
		int Green()const;
		int Blue()const;
//		int getRed();
//		int getGreen();
//		int getBlue();
};

class Segment{
	public:
		int x1, y1, x2, y2;
		Segment(int, int, int, int);
};


class Shape : public Named, public Colored{
	public:
		Shape(std::string name) : Named(name){};
		virtual std::vector<Segment> getSegments()const = 0;
	//	Named::getName();
		virtual double square() const = 0;
};

class Rectangle : virtual public Shape{
	private:
		int x1, y1, x2, y2;
	public:
		Rectangle(int width, int height) : Shape("Rectangle"){
			x1 = 0;
			y1 = 0;
			x2 = width;
			y2 = height;

		}

		Rectangle(int width, int height, int x, int y, int r, int g, int b) : Shape("Rectangle"){
			x1 = x - width / 2;
			x2 = x + width / 2;
			y1 = y - height / 2;
			y2 = y + height / 2;
			red = r;
			green = g;
			blue = b;
		}
		~Rectangle(){};
		std::vector<Segment> getSegments()const;
		double square() const;
};

class Circle : virtual public Shape{
	private:
		int x, y, r;
	public:
		Circle(int rad) : Shape("Circle"){
			r = rad;
			x = 0;
			y = 0;
		}

		Circle(int rad, int x0, int y0, int rd, int g, int b) : Shape("Circle"){
			r = rad;
			x = x0;
			y = y0;
			red = rd;
			green = g;
			blue = b;
		}
		~Circle(){};
		std::vector<Segment> getSegments()const;
		double square() const;
};

class Triangle : virtual public Shape{
	private:
		int x1, y1, x2, y2, x3, y3;
	public:
		Triangle(int x0, int y0 , int dx1 , int dy1, int dx2 , int dy2) : Shape("Triangle"){
			x1 = x0;
			y1 = y0;
			x2 = x1 + dx1;
			y2 = x1 + dy1;
			x3 = x1 + dx2;
			y3 = x1 + dy2;
		};
		Triangle(int x0, int y0, int dx1, int dy1, int dx2, int dy2, int r, int g, int b) : Shape("Triangle"){
			x1 = x0;
			y1 = y0;
			x2 = x1 + dx1;
			y2 = x1 + dy1;
			x3 = x1 + dx2;
			y3 = x1 + dy2;
			red = r;
			green = g;
			blue = b;
		}
		~Triangle(){};
		std::vector<Segment> getSegments()const;
		double square() const;	
};

void draw(image_drawer&,const Shape&);
#endif // SHAPE_H
