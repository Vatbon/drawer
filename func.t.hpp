
double totalArea(){
	return 0;
}
template<class T, class... tail>
double totalArea(const T& shp, const tail &... next){
		return shp.square() + totalArea(next...);
}
void printNames(){
	std::cout << '\n';
}
template<class T, class... tail>
void printNames(const T& shp, const tail &... next){
	std::cout << shp.getName() << " ";
	printNames(next...);
}

