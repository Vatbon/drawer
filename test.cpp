#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "shape.h"
#include "func.t.hpp"

TEST_CASE("Shape","square"){
	REQUIRE(totalArea(Rectangle(20, 20)) == 400);
	REQUIRE(totalArea(Rectangle(20, 20), Rectangle(20, 20)) == 800);
	REQUIRE(totalArea(Rectangle(20, 20), Rectangle(20, 20), Rectangle(0, 0)) == 800);
	REQUIRE(totalArea(Triangle(0, 0, 20, 0, 0, 20)) == 200);
}

TEST_CASE("Names","printNames"){
	printNames(Circle(10), Rectangle(20, 10), Rectangle(10, 20), Named("something"), Triangle(10, 10, 10, 10, 5, 10));
}

TEST_CASE("Draw Picture #2") {
	bitmap_image img(1000, 1000);
	img.set_all_channels(255, 255, 255);
	image_drawer drawer(img);
	drawer.pen_width(1);
	Circle templateCircle(200, 400, 400, 0, 0, 0);
	std::vector<Segment> pars = templateCircle.getSegments();
	size_t count = 2;
	for (auto it : pars) {
		draw(drawer, Circle(count/3, it.x1, it.y1, count / 2, count , count / 5 ));
		count++;
	}
	img.save_image("image_test2.bmp");
}
