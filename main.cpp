#include <iostream>
#include <string>
#include "bitmap/bitmap_image.hpp"
#include "shape.h"
#include "func.t.hpp"
void det_color(std::string line,int* r,int* g,int* b){
	line.erase(line.size() - 1, 1);
	if (line == "black"){
		*r = 0;
		*g = 0;
		*b = 0;
	} else
	if (line == "white"){
		*r = 255;
		*g = 255;
		*b = 255;
	} else
	if (line == "red"){
		*r = 255;
		*g = 0;
		*b = 0;
	} else
	if (line == "lime"){
		*r = 0;
		*g = 255;
		*b = 0;
	} else
	if (line == "yellow"){
		*r = 255;
		*g = 255;
		*b = 0;
	} else
	if (line == "cyan"){
		*r = 0;
		*g = 255;
		*b = 255;
	} else
	if (line == "maroon"){
		*r = 128;
		*g = 0;
		*b = 0;
	} else
	if (line == "olive"){
		*r = 128;
		*g = 128;
		*b = 0;
	} else
	if (line == "green"){
		*r = 0;
		*g = 255;
		*b = 0;
	} else
	if (line == "purple"){
		*r = 128;
		*g = 0;
		*b = 128;
	} else
	if (line == "teal"){
		*r = 0;
		*g = 128;
		*b = 128;
	} else
	if (line == "navy"){
		*r = 0;
		*g = 0;
		*b = 128;
	} else
	if (line == "magenta"){
		*r = 255;
		*g = 0;
		*b = 255;
	} else {
		*r = std::stoi(line, 0 , 10);
		line.erase(0, line.find_first_of(',', 0) + 1);
		*g = std::stoi(line, nullptr, 10);
		line.erase(0, line.find_first_of(',', 0) + 1);
		*b = std::stoi(line, nullptr, 10);
	}
}

void go(std::istream& input){
//	bitmap_image img(300, 300); // set image size
//	img.set_all_channels(255, 255, 255); // set white background`
//	image_drawer drawer(img);
//	drawer.pen_width(1);

	std::string line;
	int width, height, x0, y0, x1, y1, x2, y2, r, g, b;
	std::getline(input, line);
	width = std::stoi(line, nullptr, 10);
	line.erase(0, line.find_first_of('x', 0) + 1);
	height = std::stoi(line, nullptr, 10);

	bitmap_image img(width, height);
	img.set_all_channels(255, 255, 255); // set white background`
	image_drawer drawer(img);
	drawer.pen_width(1);
	std::vector<Shape*> shapes;
	while(std::getline(input, line)){
		if (line[0] == 'R'){
			//Rectangle(width, height) [x, y] {r, g, b}
			line.erase(0, 10);
			width = std::stoi(line, nullptr, 10);
			line.erase(0, line.find_first_of(',', 0) + 1);
			height = std::stoi(line, nullptr , 10);
			line.erase(0, line.find_first_of('[', 0) + 1);
			x0 = std::stoi(line, nullptr, 10);
			line.erase(0, line.find_first_of(',', 0) + 1);
			y0 = std::stoi(line, nullptr, 10);
			line.erase(0, line.find_first_of('{', 0) + 1);
			det_color(line, &r, &g, &b);
//			std::cout << width << " " << height << " " << x0 << " " << y0 << " " << r << g << b << '\n';
			draw(drawer, Rectangle(width, height, x0, y0, r, g, b));
		}
		if (line[0] == 'T'){
			//Triangle(x1, y1, x2, y2) [x0, y0] {r, g, b}
			line.erase(0, 9);
			x1 = std::stoi(line, nullptr, 10);
			line.erase(0, line.find_first_of(',', 0) + 1);
			y1 = std::stoi(line, nullptr, 10);
			line.erase(0, line.find_first_of(',', 0) + 1);
			x2 = std::stoi(line, nullptr, 10);
			line.erase(0, line.find_first_of(',', 0) + 1);
			y2 = std::stoi(line, nullptr, 10);
			line.erase(0, line.find_first_of('[', 0) + 1);
			x0 = std::stoi(line, nullptr, 10);
			line.erase(0, line.find_first_of(',', 0) + 1);
			y0 = std::stoi(line, nullptr, 10);
			line.erase(0, line.find_first_of('{', 0) + 1);
			det_color(line, &r, &g, &b);
//			std::cout << x1 << " " << y1 << " " << x2 << " " << y2 << " "<< x0<<" "<<y0<<" " << r << g << b << '\n';
			draw(drawer, Triangle(x0, y0, x1, y1, x2, y2, r, g, b));
		}
		if (line[0] == 'C'){
			//Circle(r) [x, y] {r, g, b}
			line.erase(0, 7);
			x1 = std::stoi(line, nullptr, 10);
			line.erase(0, line.find_first_of('[', 0) + 1);
			x0 = std::stoi(line, nullptr, 10);
			line.erase(0, line.find_first_of(',', 0) + 1);
			y0 = std::stoi(line, nullptr, 10);
			line.erase(0, line.find_first_of('{', 0) + 1);
			det_color(line, &r, &g, &b);
//			std::cout << x1 <<" " << x0 << " " << y0 << " " << r << g << b << '\n';
			draw(drawer, Circle(x1, x0, y0, r, g, b));
		}
	}
//	std::cout << totalArea(Rectangle(20, 55), Circle(50), Triangle(20, 10, 80, 40, 20, 10)) << "\n";
//	printNames(Rectangle(0 ,0));
	img.save_image("example.bmp");
}


int main(int argc, char** argv) {
	std::ifstream input_file (argv[1], std::ifstream::in);
	// draw axes

	// draw sin(x)
	if (input_file.is_open())
		go(input_file);
	else
		go(std::cin);
	return 0;
}


